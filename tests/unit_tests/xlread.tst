// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//
//   Copyright (C) 2020 - Stéphane MOTTELET
//
// This file is hereby licensed under the terms of the GNU GPL v2.0.
// For more information, see the COPYING file which you should have received
// along with this program.
//
// <-- NO CHECK REF -->

xlsFile = fullfile(SCI,'modules/spreadsheet/demos/xls/t1.xls');
xlsxFile = fullfile(SCI,'contrib/xlreadwrite/1.2/demos/xlsx/Testbig.xlsx');

// Verify XLS file
[values,text,raw] = xlread(xlsFile);
assert_checkequal(values,[1 2]);
assert_checkequal(text,["toto" "tata"]);
values = xlread(xlsFile,"Feuil2","A1:B1");
assert_checkequal(values,[33 44]);
[values,text] = xlread(xlsFile,"Feuil2","B2");
assert_checkequal(text,"1/2/2003");

// Verify XLSX file
[values,text,raw] = xlread(xlsxFile);
assert_checkequal(size(values),[998,4]);
assert_checkequal(sum(values),5147.277781427548688953);
[values,text] = xlread(xlsxFile,1,"A2:D2")
assert_checkequal(text,["X"  "sin(x)" "sin(2*x)" "cos(3*x+0.3)"])