// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//
//   Copyright (C) 2020 - Stéphane MOTTELET
//
// This file is hereby licensed under the terms of the GNU GPL v2.0.
// For more information, see the COPYING file which you should have received
// along with this program.
//
// <-- NO CHECK REF -->

xlsFile = fullfile(SCI,'modules/spreadsheet/demos/xls/t1.xls');
xlsxFile = fullfile(SCI,'contrib/xlreadwrite/1.2/demos/xlsx/Testbig.xlsx');

// Verify XLS file
[status,sheets,wbformat]=xlinfo(xlsFile);
assert_checktrue(status);
assert_checkequal(sheets,["Feuil1";"Feuil2";"Feuil3"])
assert_checkequal(wbformat,"xlWorkbookNormal")

// Verify XLSX file
[status,sheets,wbformat]=xlinfo(xlsxFile);
assert_checktrue(status);
assert_checkequal(sheets,["Sheet 1";"Sheet 2";"Sheet 3"])
assert_checkequal(wbformat,"xlOpenXMLWorkbook")
