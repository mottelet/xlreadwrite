// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//
//   Copyright (C) 2020 - Stéphane MOTTELET
//
// This file is hereby licensed under the terms of the GNU GPL v2.0.
// For more information, see the COPYING file which you should have received
// along with this program.

function xlreadwritelib = startModule()

    TOOLBOX_NAME  = "xlreadwrite";
    TOOLBOX_TITLE = "xlreadwrite";

  mprintf("Start " + TOOLBOX_TITLE + ": ");

  if isdef("xlreadwritelib") then
    warning("Toolbox xlreadwrite library is already loaded");
    return;
  end

  etc_tlbx  = get_absolute_file_path("xlreadwrite.start");
  etc_tlbx  = getshortpathname(etc_tlbx);
  root_tlbx = strncpy( etc_tlbx, length(etc_tlbx)-length("\etc\") );

//Load  functions library
// =============================================================================
  mprintf(" load macros, ");
  pathmacros = pathconvert( root_tlbx ) + "macros" + filesep();
  xlreadwritelib = lib(pathmacros);

// load Java libraries
// =============================================================================

// Initialisation of POI Libs
// Add Java POI Libs to scilab javaclasspath

path=fullfile(etc_tlbx,'..','thirdparty')+filesep();

jars=["lib/activation-1.1.1.jar"
"lib/commons-codec-1.13.jar"
"lib/commons-collections4-4.4.jar"
"lib/commons-compress-1.19.jar"
"lib/commons-logging-1.2.jar"
"lib/commons-math3-3.6.1.jar"
"lib/jaxb-api-2.3.1.jar"
"lib/jaxb-core-2.3.0.1.jar"
"lib/jaxb-impl-2.3.2.jar"
"lib/junit-4.12.jar"
"lib/log4j-1.2.17.jar"
"lib/SparseBitSet-1.2.jar"
"ooxml-lib/curvesapi-1.06.jar"
"ooxml-lib/xmlbeans-3.1.0.jar"
"poi-4.1.2.jar"
"poi-examples-4.1.2.jar"
"poi-excelant-4.1.2.jar"
"poi-ooxml-4.1.2.jar"
"poi-ooxml-schemas-4.1.2.jar"
"poi-scratchpad-4.1.2.jar"];

xlwriteClassPath=['poi_library/poi-4.1.2/'+jars];

classPath=javaclasspath();

javaclasspath(path+xlwriteClassPath);

// Load and add help chapter
// =============================================================================
  if or(getscilabmode() == ["NW";"STD"]) then
    mprintf("help.\n");
    path_addchapter = pathconvert(root_tlbx+"/jar");
    if isdir(path_addchapter) then
      add_help_chapter(TOOLBOX_NAME, path_addchapter, %F);
    end
  end


endfunction

xlreadwritelib = startModule();
clear startModule; // remove startModule on stack

